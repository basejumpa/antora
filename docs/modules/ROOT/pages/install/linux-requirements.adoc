= Linux Installation Requirements
// URLs
:url-nvm: https://github.com/creationix/nvm
:url-nvm-install: {url-nvm}#installation

On this page, you'll learn:

* [x] What tools you need in order to install Antora on Linux.
* [x] How to install the base build tools on Linux.
* [x] How to install Node 8.

If you've never installed Antora before, you need to complete the steps on this page before you can generate a documentation site.

To install Antora, you need the base build tools for your Linux distribution and Node 8.

== Base Build Tools

The base build tools are required for installing the nodegit package, a dependency of Antora.
This selection of tools provides other useful commands you'll need in your workflow, such as `git`.

Install the base build tools package by executing the command associated with your Linux distribution.

* Fedora: `dnf install @development-tools`
* Debian/Ubuntu: `apt-get install build-essential libcurl3-gnutls libssl-dev`
* Arch Linux: `pacman -S base-devel`
* Alpine Linux*: `apk add g++ libressl-dev make python curl-dev`
* RHEL*: `yum install gcc-c++ make openssl-devel libcurl-devel hostname`

{asterisk} To use Antora on Alpine Linux or RHEL, you must force NodeGit to be recompiled by setting the BUILD_ONLY=true environment variable.
See xref:install/troubleshoot-nodegit.adoc[Troubleshoot NodeGit] for details.

Once you have these tools installed, you can expect the remaining installation to go smoothly.

== Node 8

Antora requires Node 8, the current long term support (LTS) release of Node.
While you can try Node 9, Antora is not currently tested on it.

[IMPORTANT]
.Node 10 and nodegit
====
Installing Antora using Node 10 fails due to a known compatibility issue with nodegit. 

See: https://github.com/nodegit/nodegit/issues/1490
====

To check which version of Node you have installed, if any, open a terminal and type:

[source]
$ node --version

*If this command fails with an error*, you don't have Node installed.
The best way to install Node 8 is to use nvm.
See <<install-nvm>> for instructions.
If your package manager provides Node and npm packages, and you're familiar with using the tools installed system-wide, that may be a more suitable option for you.

*If the command returns a version less than 8.0.0*, upgrade to the latest Node 8 version using nvm or your package manager.

*If the command returns a Node 8 version*, set Node 8 as the default by typing the following command in your terminal.

[source]
$ nvm alias default 8

Now you're ready to xref:install/install-antora.adoc[install Antora].

[#install-nvm]
== Install nvm and Node

We recommend using the {url-nvm}[Node Version Manager (nvm)^] to manage your Node installations, especially if your package manager doesn't provide Node and npm.
Follow these {url-nvm-install}[installation instructions^] to set up nvm on your machine.

TIP: Many CI environments use nvm to install the version of Node used for the build job.
By using nvm, you can closely align your setup with the environment that is used to generate and publish your production site.

Once you've installed nvm, open a new terminal and install Node 8.

[source]
$ nvm install 8

The above command will install the latest version of Node 8 and automatically set it as your default alias.

Now that you have Node, you can xref:install/install-antora.adoc[install Antora].

[#upgrade-node]
== Upgrade Node with nvm

If you have nvm installed but your Node version is less than 8.0.0, type the following command in your terminal.

[source]
$ nvm install 8

The above command will install the latest version of Node 8.

To set the latest version of Node 8 as the default for any new terminal, type:

[source]
$ nvm alias default 8

Now that you've upgraded Node, you're ready to xref:install/install-antora.adoc[install Antora].

== What's next?

Once you've installed the base build tools and Node 8, it's time to xref:install/install-antora.adoc[install Antora].
