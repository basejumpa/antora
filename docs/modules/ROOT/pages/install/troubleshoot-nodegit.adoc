= Troubleshoot NodeGit
// URLs
:url-nodegit: http://www.nodegit.org
:url-nodegit-dev: http://www.nodegit.org/guides/install/from-source

Antora interfaces with git repositories using {url-nodegit}[nodegit^].
During installation (specifically the default site generator), Antora downloads and installs nodegit automatically.
But this step requires support from your system.
If Antora fails to install or run, nodegit is the most probable culprit.
This page helps you troubleshoot the problem.

== Check prerequisites

If installation fails when it arrives at installing nodegit, the first thing to check is that you've satisfied all prerequisites.
Specifically, you must have installed the base build tools for your system (Linux and macOS only).

* xref:install/linux-requirements.adoc#base-build-tools[Install base build tools for Linux]
* xref:install/macos-requirements.adoc#base-build-tools[Install base build tools for macOS]

If that doesn't do the trick, or Antora fails at runtime, you may either need to patch your system or force nodegit to be recompiled.

== NodeGit and Linux

The nodegit dependency may fail on certain Linux distributions when you attempt to install the default Antora site generator.

When the nodegit dependency fails, you'll see the following error output to you terminal.

....
Error: libcurl-gnutls.so.4: cannot open shared object file: No such file or directory
....

This is an open issue in nodegit.
See https://github.com/nodegit/nodegit/issues/1246[nodegit#1246^].

If you see this error, you either need to create the missing symlink or force nodegit to be recompiled instead of using a precompiled binary.
If you're using Alpine Linux, you must <<recompile-nodegit,force nodegit to recompile>>.

== Option 1: Create missing symlink

To create the missing symlink, run the following command:

 $ sudo ln -s /usr/lib64/libcurl.so.4 /usr/lib64/libcurl-gnutls.so.4

If that fails, run:

 $ sudo ln -s /usr/lib/libcurl.so.4 /usr/lib/libcurl-gnutls.so.4

Once you've made that symlink, run the `npm install` command to install the site generator.

 $ npm install -g @antora/site-generator-default

[#recompile-nodegit]
== Option 2: Recompile NodeGit

If you aren't comfortable making a system-wide change, or you're on Alpine Linux or RHEL, you can recompile nodegit on your machine instead.

Force nodegit to recompile by passing the `BUILD_ONLY=true` environment variable to the `npm install` command.

 $ BUILD_ONLY=true npm install -g @antora/site-generator-default

Be aware that recompiling nodegit will make installation take considerably longer.

== What's next?

Now that the default Antora site generator is installed, you are ready to:

* Set up a xref:playbook:index.adoc[playbook] or use the Demo playbook.
* Organize a xref:component-structure.adoc[documentation component repository] or use Antora's Demo docs components.
* xref:run-antora.adoc[Run Antora] and generate a documentation site.
