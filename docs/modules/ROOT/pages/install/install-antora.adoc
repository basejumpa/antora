= Install Antora

Assumptions:

* [x] You've installed the base build tools on your xref:install/linux-requirements.adoc#base-build-tools[Linux] or xref:install/macos-requirements.adoc#base-build-tools[macOS] machine (not applicable for Windows).
* [x] You've installed Node 8 on your xref:install/linux-requirements.adoc#node-8[Linux], xref:install/windows-requirements.adoc#node-8[Windows], or xref:install/macos-requirements.adoc#node-8[macOS] machine.

On this page, you'll learn:

* [x] How to install the Antora CLI.
* [x] How to install the default Antora site generator.

To generate documentation sites with Antora, you need the Antora command line interface (CLI) and an Antora site generator pipeline.
Once these packages are installed, you can use the `antora` command to publish a site.

== Install the Antora CLI

We'll begin by installing the Antora CLI using npm, the package manager for Node.
This package manager was installed when you installed Node.

Open a terminal and type:

 $ npm i -g @antora/cli

IMPORTANT: The `@` at the beginning of the package name is important.
It tells `npm` that the `cli` package is located in the `antora` group.
If you omit this character, `npm` will assume the package name is the name of a git repository on GitHub.

We recommend installing (`i` subcommand) the CLI package globally (`-g` flag) so the `antora` command, which is provided by the CLI package, becomes available on your PATH.

When we say "`globally`" here, it does not imply system-wide.
It means the location where Node is installed.
If you used nvm to install Node, this location will be inside your home directory (thus not requiring elevated permissions).

If you prefer Yarn over npm, use this command instead:

 $ yarn global add @antora/cli

CAUTION: If you're using a system-wide Node installation managed by a package manager, you may run into permission problems when installing packages.
In that case, just drop the `-g` flag so that the package is installed under the [.path]_node_modules_ folder in the current directory.

Verify the `antora` command is available on your PATH by running:

 $ antora -v

If you installed the package locally, meaning without the `-g` flag, you'll need to prefix the command as follows:

 $ $(npm bin)/antora

If installation was successful, the command should report the version of Antora.

[subs=attributes+]
 $ antora -v
 {page-component-version}.x

== Install the default Antora site generator

Next, install the default site generator.

To install the generator globally, type:

 $ npm i -g @antora/site-generator-default

IMPORTANT: The `@` at the beginning of the package name is important.
It tells `npm` that the `cli` package is located in the `antora` group.
If you omit this character, `npm` will assume the package name is the name of a git repository on GitHub.

If you prefer Yarn over npm, use this command instead:

 $ yarn global add @antora/site-generator-default

If you don't want to install the generator globally (or run into permission problems trying), you can opt to install it inside the playbook project (the project that contains the playbook file(s) for your site).

Switch to the playbook project and type:

 $ npm i @antora/site-generator-default

CAUTION: If you're on Linux and get an error message about `libcurl-gnutls.so.4`, you'll need to xref:install/troubleshoot-nodegit.adoc[patch or recompile nodegit].

== What's next?

Now that the Antora CLI and default site generator are installed, you are ready to:

* Set up your own xref:playbook:index.adoc[playbook] or use the Demo playbook.
* Organize a xref:component-structure.adoc[documentation component repository] or use Antora's Demo docs components.
* xref:run-antora.adoc[Run Antora] and generate a documentation site.
